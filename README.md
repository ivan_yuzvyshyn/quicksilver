﻿

Installation
------------

Using the extension package for Episerver CMS you can set up a project with a database, required NuGet references, and sample templates (optional). The easiest way to install the Episerver Visual Studio extensions is directly from Visual Studio (Community, Professional or Enterprise) as described below: 

Select Tools > Extensions and Updates.
In the extensions window, select Online, and type "episerver" in the search box.
Select Episerver CMS Visual Studio Extension and click Install (or Update if already installed).


1.  Configure Visual Studio to add this package source: http://nuget.episerver.com/feed/packages.svc/. This allows missing packages to be downloaded, when the solution is built.
2.  Open solution and build to download nuget package dependencies.
3.  Search the solution for "ChangeThis" and review/update as described.
4.  Run Setup\SetupDatabases.cmd to create the databases *. In the unlucky event of errors please check the logs.  
5.  Start the site (Debug-Start from Visual studio) and browse to http://localhost:50244 to finish installation. Login with admin@example.com/store.

*By default SetupDatabases.cmd use the default SQL Server instance. Change this line `set sql=sqlcmd -S . -E` by replacing `.` with the instance name to use different instance.

Note: SQL scripts are executed using Windows authentication so make sure your user has sufficient permissions.



SQL Server authentication
-------------------------

If you don't have mixed mode authentication enabled you can edit this line in SetupDatabases.cmd and provide username and password.

```
set sql=sqlcmd -S . -U username -P password
```