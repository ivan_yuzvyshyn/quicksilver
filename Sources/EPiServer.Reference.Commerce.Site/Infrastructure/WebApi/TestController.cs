﻿using EPiServer.Commerce.Marketing;
using EPiServer.Commerce.Order;
using Mediachase.Commerce.Orders;
using EPiServer.Reference.Commerce.Site.Features.Cart.Services;
using EPiServer.Reference.Commerce.Site.Features.Cart.ViewModelFactories;
using EPiServer.Reference.Commerce.Site.Infrastructure.Facades;
using EPiServer.Web.Mvc;
using Mediachase.Commerce.Orders.Search;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using Mediachase.Commerce.Website.Helpers;

namespace EPiServer.Reference.Commerce.Site.Infrastructure.WebApi
{
    [RoutePrefix("episerverapi/test")]
    [Authorize]
    public class TestController : ApiController
    {
        private readonly ICartService _cartService;
        private readonly CustomerContextFacade _customerContext;
        private readonly IOrderRepository _orderRepository;
        private readonly IPromotionEngine _promotionEngine;
        readonly CartViewModelFactory _cartViewModelFactory;

        public TestController(
            CustomerContextFacade customerContextFacade
            , IOrderRepository orderRepository
            , ICartService cartService
            , IPromotionEngine promotionEngine
            , CartViewModelFactory cartViewModelFactory
            )
        {
            _customerContext = customerContextFacade;
            _orderRepository = orderRepository;
            _promotionEngine = promotionEngine;
            _cartViewModelFactory = cartViewModelFactory;
            _cartService = cartService;
        }


        [HttpGet]
        [Route("Get")]
        [AuthorizePermission("EpiServerServiceApi", "ReadAccess")]
        public virtual IHttpActionResult Get()
        {

            return Ok(new[]
            {
                "Hello World"
            });

        }
        [HttpGet]
        [Route("GetCart")]
        [AuthorizePermission("EPiServerServiceApi", "ReadAccess")]
        [ResponseType(typeof(Cart))]
        public virtual IHttpActionResult GetCart()
        {
            try
            {
                //var cart = _cartService.LoadCart(_cartService.DefaultCartName);

                CartHelper cart = new CartHelper(Cart.DefaultName);


                return Ok(cart.LineItems);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }


        [HttpGet]
        [Route("GetWishList")]
        [AuthorizePermission("EPiServerServiceApi", "ReadAccess")]
        public virtual IHttpActionResult GetWishList()
        {
            try
            {

                //var cart = _cartService.LoadCart(_cartService.DefaultWishListName);
                //var cart = OrderContext.Current.GetCart(_cartService.DefaultCartName, _customerContext.CurrentContactId);

                CartHelper cart = new CartHelper(Cart.WishListName);

                return Ok(cart.LineItems);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        [HttpGet]
        [Route("GetOrders")]
        [AuthorizePermission("EPiServerServiceApi", "ReadAccess")]
        public virtual IHttpActionResult GetOrders()
        {
            try
            {
                var purchaseOrderCollection = OrderContext.Current.GetPurchaseOrders(_customerContext.CurrentContactId);
                return Ok(purchaseOrderCollection);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
