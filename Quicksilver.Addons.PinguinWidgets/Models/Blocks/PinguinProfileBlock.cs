﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Quicksilver.Addons.PinguinWidgets.Models.Blocks
{
    [ContentType(DisplayName = "PinguinProfileBlock", GUID = "786d514f-5c63-443f-baa7-0a96f52f0056", Description = "")]
    public class PinguinProfileBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Name",
            Description = "Name field's description",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Name { get; set; }

    }
}