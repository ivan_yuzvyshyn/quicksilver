﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Shell;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using Quicksilver.Addons.PinguinWidgets.Models.Blocks;

namespace Quicksilver.Addons.PinguinWidgets.Controllers
{
    public class PinguinProfileBlockController : BlockController<PinguinProfileBlock>
    {
        public override ActionResult Index(PinguinProfileBlock currentBlock)
        {
            return PartialView(Paths.ToResource(this.GetType(),
            "Views/PinguinProfile/PinguinProfile.cshtml"), currentBlock);
        }
    }
}
